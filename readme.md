# lambdaR
A portable R deployment.

## Source

https://www.linkedin.com/pulse/running-r-statistics-aws-lambda-ignacio-ocampo/

## Instructions
To deploy, simply upload the zip folder to your AWS lambda function.

# Reproduce

Follow these steps to reproduce and install your own packages.

*Reproduction steps assume you're on a Ubuntu system, use a VM image from [osboxes](http://www.osboxes.org/ubuntu/) if you are currently on something else.

### Install R
```{bash}
sudo echo 'deb http://cran.utstat.utoronto.ca/bin/linux/ubuntu xenial/' | sudo tee -a /etc/apt/sources.list

sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E084DAB9

sudo apt-get update
sudo apt-get install r-base
sudo apt-get install r-base-dev
sudo apt-get install r-cran-littler
```

### Install Package Deps
```{bash}
sudo apt-get install libcurl4-openssl-dev
```

Then create a new file named `install_r_packages.R`

```{R}
#!/usr/bin/env r

if (is.null(argv) | length(argv)<1) {
  cat("Usage: installr.r pkg1 [pkg2 pkg3 ...]\n")
  q()
}

## adjust as necessary, see help('download.packages')
repos <- "http://cran.rstudio.com"

lib.loc <- "~/portable/R/library"

install.packages(argv, lib.loc, repos)
```

Make executable:
```{bash}
sudo chmod +x install_r_packages.R
sudo mv install_r_packages.R /usr/bin/install_r_packages.R
```

Make a portable R directory
```{bash}
mkdir -p ~/portable/R/library
```

Install packages:
```{bash}
sudo install_r_packages.R data.table base64enc tidyr purrr lubridate haven jsonlite
```


Override environment variables to point to portable folder. Edit as needed to match your install_r_pacakges.R script.
```{bash}
export R_PORTABLE="/home/osboxes/portable"
```

Remaining build off of R_PORTABLE.
```{bash}
export R_HOME=$R_PORTABLE"/R"
export R_HOME_DIR=$R_HOME
export R_LIBS_SITE=$R_HOME"/site-library"
export LD_LIBRARY_PATH=$R_HOME"/lib"
```


Copy R installation to portable folder.
```{bash}
cp -Lr /usr/lib/R $R_HOME
```

Comment out hard coded R_HOME.
```{bash}
sed -i 's/R_HOME_DIR=\/usr\/lib\/R/#R_HOME_DIR=\/usr\/lib\/R/' $R_HOME/bin/R
```

Move shared object files as well. Use a little r script named `move_so_deps.R`
```{R}
#!/usr/bin/env r

deps <- "/usr/lib/libblas.so.3
/lib/x86_64-linux-gnu/libreadline.so.6
/usr/lib/x86_64-linux-gnu/libgomp.so.1
/lib/x86_64-linux-gnu/libbz2.so.1.0
/usr/lib/x86_64-linux-gnu/libgfortran.so.3
/usr/lib/x86_64-linux-gnu/libquadmath.so.0
/usr/lib/liblapack.so.3
/lib/x86_64-linux-gnu/libpcre.so.3
/lib/x86_64-linux-gnu/libtinfo.so.5
/lib/x86_64-linux-gnu/libm.so.6"

lapply(strsplit(deps, split = "\n", fixed = TRUE)[[1]], function(el) {
    s_command <- paste0("cp -L ", el, " $LD_LIBRARY_PATH")
    # system("mkdir -p $LD_LIBRARY_PATH")
    system(s_command)
})
```

Change its permissions.
```{bash}
sudo chmod +x move_so_deps.R
```

and run it
```{bash}
./move_so_deps.R
```

Change owner.
```{bash}
sudo chown -R $USER: $R_HOME/library
```

Delete BH folder (too big to fit and only used to compile packages)
```{bash}
sudo rm -rf /home/osboxes/portable/R/library/BH
```


Test using the following as script.js.
```{JavaScript}
'use strict';

// Required to call the R binary
var exec = require('child_process').exec;
var fs = require('fs');

// The name of the R script
var rScript = 'script.R'

// Setup the environment variables
process.env['LD_LIBRARY_PATH'] = process.env['LAMBDA_TASK_ROOT'] + '/R/lib';
process.env['R_HOME'] = process.env['LAMBDA_TASK_ROOT'] + '/R/';
process.env['R_HOME_DIR'] = process.env['LAMBDA_TASK_ROOT'] + '/R/';
process.env['R_LIBS_SITE'] = process.env['LAMBDA_TASK_ROOT'] + '/R/site-library' + ':' + process.env['LAMBDA_TASK_ROOT'] + '/R/library';

// Lambda handler
exports.handler = function (event, context) {
    console.log("Running R script");
    
    // Call to R binary with the path of the script
    var commandLineCall = './R/bin/exec/R --slave --file=' +
        process.env['LAMBDA_TASK_ROOT'] +
        '/' + rScript + 
        ' --args ' + 
        JSON.stringify(event.expr);

    exec(commandLineCall, function(error, stdout) {
        if (error) { console.log(error); }
        context.succeed(JSON.parse(stdout));
    });
};
```


And the following as script.R
```{R}
args <- commandArgs(TRUE)
    
# json <- jsonlite::fromJSON(args[1])

res <- eval(parse(text = args[1]))

cat(jsonlite::toJSON(res, auto_unbox=TRUE))
```

Example request payload
```{JSON}
{
  "expr": "data.table::data.table(mtcars)[, .N, by=cyl]"
}
```

or 

```{JSON}
{
  "expr": "with(lm(formula = mpg ~ ., data = mtcars), list(coefficients = coefficients, residuals = residuals))"
}
```